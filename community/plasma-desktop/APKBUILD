# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-desktop
pkgver=5.27.2
pkgrel=0
pkgdesc="KDE Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://kde.org/plasma-desktop/'
license="GPL-2.0-only AND LGPL-2.1-only"
depends="
	accountsservice
	font-noto-emoji
	ibus-emoji
	kirigami2
	plasma-workspace
	qqc2-desktop-style
	setxkbmap
	xdg-user-dirs
	"
makedepends="
	attica-dev
	baloo-dev
	eudev-dev
	extra-cmake-modules
	fontconfig-dev
	ibus-dev
	kaccounts-integration-dev
	kactivities-dev
	kactivities-stats-dev
	kauth-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdelibs4support-dev
	kdoctools-dev
	kemoticons-dev
	kglobalaccel-dev
	ki18n-dev
	kitemmodels-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kpeople-dev
	kpipewire-dev
	krunner-dev
	kwallet-dev
	kwin-dev
	libxcursor-dev
	libxi-dev
	libxkbfile-dev
	plasma-framework-dev
	plasma-workspace-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	signon-plugin-oauth2-dev
	xf86-input-evdev-dev
	xf86-input-libinput-dev
	xf86-input-synaptics-dev
	xkeyboard-config-dev
	"
checkdepends="xvfb-run iso-codes"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-desktop-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-lang knetattach"
options="!check" # Requires running dbus

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

knetattach() {
	pkgdesc="Wizard which makes it easier to integrate network resources with the Plasma Desktop"
	depends="kdelibs4support"

	amove usr/bin/knetattach
	amove usr/share/applications/org.kde.knetattach.desktop
}

sha512sums="
adc9d4ba9dee1749c0ce63b920d5f3346fd2d260c4253457f5ea3aa79fab4343b7b77a8d05923e56c3f50987a4f734e3e2a4329d9a80608fb0076bca4c3de98a  plasma-desktop-5.27.2.tar.xz
"
