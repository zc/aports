# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bluedevil
pkgver=5.27.2
pkgrel=0
pkgdesc="Integrate the Bluetooth technology within KDE workspace and applications"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends="
	bluez
	bluez-obexd
	kded
	"
makedepends="
	bluez-qt-dev
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kded-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	shared-mime-info
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/bluedevil-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7c0fc8ac757cd7d48450d11a4f99ee20f1cee5340a7bf7db31cfe69b25e35067c17375828eb9bc83b69543b6660e423b64e6459400b315c4e2cf9b405a3483bd  bluedevil-5.27.2.tar.xz
"
