# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=roc-toolkit
pkgver=0.2.2
pkgrel=0
pkgdesc="Real-time audio streaming over the network"
url="https://roc-streaming.org/"
arch="all"
license="MPL-2.0"
depends_dev="
	libunwind-dev
	libuv-dev
	speexdsp-dev
	"
makedepends="
	$depends_dev
	cpputest
	gengetopt
	ragel
	scons
	"
checkdepends="
	python3
	"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc"
source="https://github.com/roc-streaming/roc-toolkit/archive/v$pkgver/roc-toolkit-$pkgver.tar.gz"
options="net" # Required for tests

case "$CARCH" in
arm*|x86)
	# fail on 32-bit
	# original: -0.999969,	received: -0.993927
	options="$options !check"
esac

_run_scons() {
	scons \
		--prefix=/usr \
		--with-libraries=/usr/lib \
		--enable-tests \
		--disable-sox \
		--disable-openfec \
		--disable-pulseaudio \
		"$@"
}

build() {
	export CFLAGS="$CFLAGS -O2 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -O2 -flto=auto"
	_run_scons
}

check() {
	_run_scons test
}

package() {
	export DESTDIR="$pkgdir"
	_run_scons install
}

sha512sums="
b39a88811c3d7c8dfbaeb73f22d24715240c0e7ce0ae6b9148980a520efc0cf4939e1d461f05c7f00b15282756826c145a5560b2cbce8373ecc13b4ad93c47ee  roc-toolkit-0.2.2.tar.gz
"
